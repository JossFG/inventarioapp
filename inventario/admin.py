from django.contrib import admin
from django import forms
from pyxlsb import open_workbook

# Register your models here.
from .models import Inventario
from .models import Archivo

class ArchivoForm(forms.ModelForm):

    def save(self, commit=True):
        cantidad = 0
        precio = 0
        with open_workbook(self.cleaned_data['documento']) as wb:
            with wb.get_sheet(1) as sheet:
                for row in sheet.rows(sparse=True):
                    if row[0].r != 0:
                        try:
                            num_serie = str(row[0].v)
                            cantidad = int(row[1].v)
                            precio = float(row[2].v)
                        except: 
                            num_serie = None
                        if num_serie is not None:
                            inv = Inventario(numero_serie=num_serie,cantidad=cantidad, precio=precio)
                            inv.save()
           
        return super(ArchivoForm, self).save(commit=commit)

    class Meta:
        model = Archivo
        fields = '__all__'

@admin.register(Archivo)
class ArchivoAdmin(admin.ModelAdmin):
    form = ArchivoForm
    list_display = ('nombre','info' )
    ordering = ('nombre',)
    search_fields = ('nombre', 'documento')

    fieldsets = (
        (None, {
            'fields': ('nombre', 'documento'),
        }),
    )

admin.site.register(Inventario)
#admin.site.register(Archivo)