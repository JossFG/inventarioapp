from django.db import models
from django.utils import timezone
from pyxlsb import open_workbook
import json;

# Create your models here.
class Inventario(models.Model):
    numero_serie = models.CharField(max_length=40)
    cantidad = models.IntegerField(default=0)
    precio = models.DecimalField(decimal_places=4, max_digits=9, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.fecha, filename)

class Archivo(models.Model):
    nombre = models.CharField(max_length=60)
    fecha = models.DateField(auto_now=True)
    documento = models.FileField(upload_to='archivos/')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    @property
    def info(self):
        suma = 0
        precio = 0
        total = 0
        promedio = 0
        cantidad = 0
        with open_workbook(self.documento) as wb:
            with wb.get_sheet(1) as sheet:
                for row in sheet.rows(sparse=True):
                    if row[0].r != 0:
                        try:
                            suma += int(row[1].v)
                        except:
                            suma += 0
                        try:
                            precio += float(row[2].v)
                        except:
                            precio += 0
                        cantidad += 1
        promedio = precio / cantidad
        return json.dumps({'elementos': suma, 'promedio': promedio}, sort_keys=True, indent=4)
